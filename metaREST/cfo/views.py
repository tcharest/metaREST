from cfo.models import CFO as CFOModel
from cfo.serializers import CFOSerializer
from django.http import Http404
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status

class CFO(APIView):
    def get_object(self, id):
        try:
            return CFOModel.objects.get(id=id)
        except CFOModel.DoesNotExist:
            raise Http404
    
    def get(self, request, id=-1):
        if id == -1:
            cfo_inst   = CFOModel.objects.all()
            serializer = CFOSerializer(cfo_inst, many=True)
        else:
            cfo_inst   = self.get_object(id)
            serializer = CFOSerializer(cfo_inst)
        return Response(serializer.data)
    
    def post(self, request, format=None):
        serializer = CFOSerializer(data=request.data)
        if serializer.is_valid():
            serializer.create(request.data)
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
