from __future__ import unicode_literals

from django.db import models

class CFO(models.Model):
    id            = models.AutoField(primary_key=True)
    max_time      = models.IntegerField()
    func          = models.CharField(max_length=50)
    dimension     = models.IntegerField()
    distribution  = models.IntegerField()
    max_x         = models.FloatField()
    max_y         = models.FloatField()
    alpha         = models.FloatField()
    beta          = models.FloatField()
    gamma         = models.FloatField()
    accuracy      = models.FloatField()
    probes        = models.CharField(max_length=10000000, default="[]")
    probe_density = models.PositiveIntegerField(null=True) 