from cfo.models import CFO
from rest_framework import serializers

class CFOSerializer(serializers.ModelSerializer):
    probes = serializers.JSONField()
    id = serializers.ReadOnlyField()
    class Meta:
        model  = CFO
        
        fields = (
            'id',
            'max_time',
            'func','dimension', 
            'distribution', 
            'max_x', 
            'max_y', 
            'alpha', 
            'beta', 
            'gamma', 
            'accuracy', 
            'probes',
            'probe_density' )
